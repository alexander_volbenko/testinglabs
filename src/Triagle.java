import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Triagle {

    public static void main(String[] args) {
        System.out.println(typeOfTriagle(inpute()));
    }

    private static float[] inpute() {
        Scanner sc = new Scanner(System.in);
        float[] sides = new float[3];
        boolean sw = false;
        while (!sw) {
            for (int i = 0; i < 3; ) {
                float side;
                try {
                    side = sc.nextFloat();
                    if (side > 0) {
                        sides[i] = side;
                        i++;
                    } else {
                        System.out.println("Неверное значение");
                    }
                } catch (InputMismatchException e) {
                    System.out.println("Неверный тип данных");
                    sc.nextLine();
                }
            }
            if (sides[0] + sides[1] > sides[2] && sides[0] + sides[2] > sides[1] && sides[1] + sides[2] > sides[0]) {
                sw = true;
            } else {
                System.out.println("Невозможный треугольник");
            }
        }
        return sides;
    }

    public static String typeOfTriagle(float[] sides) {
        Arrays.sort(sides);
        if ((Math.pow(sides[0], 2) + Math.pow(sides[1], 2) == Math.pow(sides[2], 2)))
            return "Прямоугольный";
        if (sides[0] == sides[1] || sides[1] == sides[2]) {
            if (sides[0] == sides[2]) return "Равносторонний";
            else return "Равнобедренный";
        } else return "Неравносторонний";
    }
}

