import java.util.InputMismatchException;
import java.util.Scanner;

public class Quadrur {

    public static void main(String[] args) {
        System.out.println(calculation(Input()));
    }

    private static double[] Input() {
        Scanner sc = new Scanner(System.in);
        double[] numbers = new double[3];
        for (int i = 0; i < 3; ) {
            double number;
            try {
                number = sc.nextDouble();
                if (number != 0) {
                    numbers[i] = number;
                    i++;
                } else {
                    System.out.println("Не может быть 0");
                }
            } catch (InputMismatchException e) {
                System.out.println("Неверный тип данных");
                sc.nextLine();
            }
        }
        return numbers;
    }

    public static String calculation(double[] numbers) {
        double d = Math.pow(numbers[1], 2) - (4 * numbers[0] * numbers[2]);
        if (d > 0)
            return (-numbers[1] + Math.sqrt(d)) / (2 * numbers[0]) + ";" + (-numbers[1] - Math.sqrt(d)) / (2 * numbers[0]);
        if (d == 0) return String.valueOf(-numbers[1] / (2 * numbers[0]));
        else return "Корней нет";
    }
}
