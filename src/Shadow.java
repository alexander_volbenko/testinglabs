import java.util.*;

public class Shadow {

    public static void main(String[] args) {
        int result = sumShadow(shadow(inpute()));
        System.out.println(result);
    }

    private static ArrayList inpute() {
        Scanner sc = new Scanner(System.in);
        ArrayList array = new ArrayList();
        for (int i = 0; i < 3; ) {
            int x1;
            int x2;
            try {
                x1 = sc.nextInt();
                x2 = sc.nextInt();
                if (x2 > x1) {
                    array.add(x1);
                    array.add(x2);
                    i++;
                } else {
                    System.out.println("x2 !< x1");
                }
            } catch (InputMismatchException e) {
                System.out.println("Неверный тип данных");
                sc.nextLine();
            }
        }
        return array;
    }

    public static int sumShadow(ArrayList<Integer> shadows) {
        int result = 0;
        
        for (int i = 0; i < shadows.size(); i += 2) {
            result += shadows.get(i+1)-shadows.get(i);
        }

        return result;
    }

    public static ArrayList shadow(ArrayList<Integer> first) {

        ArrayList<Integer> result = new ArrayList();
        result.add(first.get(0));
        result.add(first.get(1));

        for (int i = 0; i < first.size(); i += 2) {
            boolean newShadow = false;
            for (int j = 0; j < result.size(); j += 2) {
                if (first.get(i) > result.get(j + 1) || first.get(i + 1) < result.get(j)) {
                    newShadow = true;
                } else {
                    newShadow = false;
                    break;
                }
            }
            if (newShadow) {
                result.add(first.get(i));
                result.add(first.get(i + 1));
            } else {
                for (int j = 0; j < result.size(); j += 2) {
                    if (first.get(i) < result.get(j) && first.get(i + 1) >= result.get(j))
                        result.set(j, first.get(i));
                    if (first.get(i + 1) > result.get(j + 1) && first.get(i) <= result.get(j + 1))
                        result.set(j + 1, first.get(i + 1));
                }
            }
        }

        for (int i = 0; i < result.size(); i += 2) {
            for (int j = 0; j < result.size(); j += 2) {
                if (i != j) {
                    if (result.get(i) == result.get(j) && result.get(i + 1) == result.get(j + 1)) {
                        result.remove(j + 1);
                        result.remove(j);
                    } else {
                        if (result.get(i) < result.get(j) && result.get(i + 1) >= result.get(j))
                            result.set(j, result.get(i));
                        if (result.get(i + 1) > result.get(j + 1) && result.get(i) <= result.get(j + 1))
                            result.set(j + 1, result.get(i + 1));
                    }
                }
            }
        }

        return result;
    }
}

