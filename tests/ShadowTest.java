import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ShadowTest {

    @Test
    public void sumShadow() {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(0);
        numbers.add(3);
        numbers.add(1);
        numbers.add(2);
        assertEquals(4, Shadow.sumShadow(numbers));
    }

    @Test
    public void shadow(){
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(0);
        numbers.add(3);
        numbers.add(1);
        numbers.add(4);
        numbers.add(6);
        numbers.add(7);
        ArrayList<Integer> result = new ArrayList<>();
        result.add(0);
        result.add(4);
        result.add(6);
        result.add(7);
        assertEquals(result,Shadow.shadow(numbers));
    }
}