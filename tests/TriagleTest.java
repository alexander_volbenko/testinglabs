import org.junit.Test;

import static org.junit.Assert.*;

public class TriagleTest {

    @Test
    public void ravnb() {
        String result = "Равнобедренный";
        float[] sides = {3.0f, 3.0f, 4.0f};
        assertEquals(result, Triagle.typeOfTriagle(sides));
    }

    @Test
    public void ravns() {
        String result = "Равносторонний";
        float[] sides = {3.0f, 3.0f, 3.0f};
        assertEquals(result, Triagle.typeOfTriagle(sides));
    }

    @Test
    public void unravns() {
        String result = "Неравносторонний";
        float[] sides = {1.0f, 2.0f, 4.0f};
        assertEquals(result, Triagle.typeOfTriagle(sides));
    }

    @Test
    public void pryamougl() {
        String result = "Прямоугольный";
        float[] sides = {4.0f, 3.0f, 5.0f};
        assertEquals(result, Triagle.typeOfTriagle(sides));
    }
}