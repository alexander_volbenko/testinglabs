import org.junit.Test;

import static org.junit.Assert.*;

public class QuadrurTest {

    @Test
    public void calculationDPlus() {
        double[] numbers = {3.0, 3.0, 4.0};

        String result;

        double d = Math.pow(numbers[1], 2) - (4 * numbers[0] * numbers[2]);
        if (d > 0)
            result = (-numbers[1] + Math.sqrt(d)) / (2 * numbers[0]) + ";" + (-numbers[1] - Math.sqrt(d)) / (2 * numbers[0]);
        if (d == 0)  result = String.valueOf(-numbers[1] / (2 * numbers[0]));
        else result = "Корней нет";

        assertEquals(result, Quadrur.calculation(numbers));
    }

    @Test
    public void calculationDMinus() {
        double[] numbers = {4, 1, 2};

        String result;

        double d = Math.pow(numbers[1], 2) - (4 * numbers[0] * numbers[2]);
        if (d > 0)
            result = (-numbers[1] + Math.sqrt(d)) / (2 * numbers[0]) + ";" + (-numbers[1] - Math.sqrt(d)) / (2 * numbers[0]);
        if (d == 0)  result = String.valueOf(-numbers[1] / (2 * numbers[0]));
        else result = "Корней нет";

        assertEquals(result, Quadrur.calculation(numbers));
    }

    @Test
    public void calculationDZero() {
        double[] numbers = {1, 2, 1};

        String result;

        double d = Math.pow(numbers[1], 2) - (4 * numbers[0] * numbers[2]);
        if (d > 0)
            result = (-numbers[1] + Math.sqrt(d)) / (2 * numbers[0]) + ";" + (-numbers[1] - Math.sqrt(d)) / (2 * numbers[0]);
        if (d == 0)  result = String.valueOf(-numbers[1] / (2 * numbers[0]));
        else result = "Корней нет";

        assertEquals(result, Quadrur.calculation(numbers));
    }


}